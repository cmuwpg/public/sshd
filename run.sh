#!/bin/bash

mkdir -p /run/sshd
chmod 0755 /run/sshd
exec /usr/sbin/sshd -D -e -f /etc/ssh/config/sshd_config
