FROM debian:buster-slim

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get -qq update && apt-get -qq -y --no-install-recommends install \
    libnss-sss libpam-sss openssh-server openssh-sftp-server 

# Read-only home directories 
RUN echo "session required pam_mkhomedir.so skel=/etc/skel/ umask=0277" >> /etc/pam.d/sshd

# Remove host keys, because we want them to be different ... we'll define
# them in Kubernetes.
RUN rm /etc/ssh/ssh_host_*

COPY sshd_config /etc/ssh/config/sshd_config
COPY run.sh /run.sh

ENTRYPOINT ["/run.sh"]
