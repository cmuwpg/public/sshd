#/bin/sh

# First parameter is the name of the secret to create.

t=$(mktemp -d)
mkdir -p $t/etc/ssh

ssh-keygen -A -f $t

cd $t/etc/ssh

kubectl create secret generic $1 \
    --from-file=ssh_host_ecdsa_key \
    --from-file=ssh_host_ecdsa_key.pub \
    --from-file=ssh_host_ed25519_key \
    --from-file=ssh_host_ed25519_key.pub \
    --from-file=ssh_host_rsa_key \
    --from-file=ssh_host_rsa_key.pub
