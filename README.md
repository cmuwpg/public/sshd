# sshd

Use ssh inside a container.

You can use keygen.sh to create a Kubernetes secret with the SSH server's keys,
in the current namespace.

    ./keygen.sh secret-name

You'll need to mount the directory with the keys at /etc/ssh/keys

The ssh server is started with the config file at /etc/ssh/config/sshd_config

We install libpam_sss and libnss_sss, so you can do sss-based authentication.
The easiest way is to mount /var/lib/sss/pipes from the the host -- that's
literally all you have to do, which is kind of neat. That way, you'll just
inherit the authentication configuration from the host. For Active Directory
joined machines, that's very convenient, since you don't need to join the
container.
